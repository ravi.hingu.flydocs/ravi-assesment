import { IsNotEmpty, IsUUID, IsString, IsEmail, IsBoolean } from 'class-validator';

export class CreateUser {
  @IsNotEmpty()
  @IsString()
  name: string;

  @IsNotEmpty()
  @IsString()
  title: string;

  @IsNotEmpty()
  @IsUUID()
  uinqId: string;

  @IsEmail()
  @IsString()
  email: string;

  @IsNotEmpty()
  @IsBoolean()
  isGraduate: boolean;

}